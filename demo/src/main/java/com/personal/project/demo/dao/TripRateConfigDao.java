package com.personal.project.demo.dao;

import com.personal.project.demo.constants.LocationEnum;
import com.personal.project.demo.constants.VehicleType;
import com.personal.project.demo.modal.TripRateConfig;

/**
 * TripRateConfig DAO functionality
 *
 * Copyright: NA
 *
 * @author Vaibhav.Srivastava
 * @since 1.0.0
 */
public interface TripRateConfigDao {

    /**
     * get TripRateConfig
     * @param vehicleType
     * @return TripRateConfig
     */
    TripRateConfig getTripDateConfig(VehicleType vehicleType, LocationEnum destination);

}
