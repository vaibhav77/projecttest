package com.personal.project.demo.service.expensecalculator;



import com.personal.project.demo.constants.FuelType;
import com.personal.project.demo.constants.LocationEnum;
import com.personal.project.demo.constants.VehicleType;
import com.personal.project.demo.dao.TripRateConfigDao;
import com.personal.project.demo.modal.TripRateConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * This class implements the interface to calculate expense.
 *
 * Copyright: None
 *
 * @author Vaibhav.Srivastava
 * @since 1.0.0
 */
@Service("expenseCalculatorService")
public class ExpenseCalculatorImpl implements ExpenseCalculator {


    private TripRateConfigDao tripRateConfigDao;

    @Autowired
    public ExpenseCalculatorImpl(TripRateConfigDao tripRateConfigDao) {
        this.tripRateConfigDao = tripRateConfigDao;
    }

    /**
     * @param vehicleType               The type of selected vehicle
     *                                  (Car/Bus/Van etc.)
     * @param fuelType                  The type of selected fuel
     *                                  (Diesel /
     *                                  Petrol).
     * @param destination               The selected destination
     * @param numberOfPeopleTravelling  The number of people
     *                                  traveling
     * @param isAirConditioningRequired The selected option of air
     *                                  condition
     * @return
     */
    @Override
    public BigDecimal calculateExpense(VehicleType vehicleType, FuelType
            fuelType, String destination, Integer numberOfPeopleTravelling, Boolean
                                               isAirConditioningRequired) {

        TripRateConfig tripRateConfig = tripRateConfigDao.getTripDateConfig(vehicleType, LocationEnum.getEnum(destination));
        BigDecimal tripRate = tripRateConfig.getStandardFuelRate();
        if (fuelType.equals(FuelType.DIESEL)){
            tripRate = tripRate.subtract(tripRateConfig.getDieselDiscount());
        }
        if(isAirConditioningRequired){
            tripRate = tripRate.add(tripRateConfig.getAcCharge());
        }
        if(vehicleType.equals(VehicleType.BUS)){
            tripRate = tripRate.subtract(tripRate.multiply(tripRateConfig.getBusDiscount()).scaleByPowerOfTen(-2));
        }

        Integer extraPerson = numberOfPeopleTravelling - tripRateConfig.getVehicleCapacity();
        if(extraPerson > 0){
            tripRate = tripRate.add(tripRateConfig.getExtraPersonCharge().multiply(BigDecimal.valueOf(extraPerson)));
        }
        return  tripRate.multiply(tripRateConfig.getDistance());
    }

}
