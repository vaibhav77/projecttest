package com.personal.project.demo.dao;


import com.personal.project.demo.constants.LocationEnum;
import com.personal.project.demo.constants.VehicleType;
import com.personal.project.demo.modal.TripRateConfig;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
 * Default admin dao implementation.
 *
 * Copyright: NA
 *
 * @author Vaibhav.Srivastava
 * @since 1.0.0
 */
@Repository
public class TripRateConfigDaoImpl implements TripRateConfigDao {


    @Override
    public TripRateConfig getTripDateConfig(VehicleType vehicleType, LocationEnum location) {
        TripRateConfig tripRateConfig = new TripRateConfig(new BigDecimal("15"), new BigDecimal("1"),
                getBusDiscount(vehicleType),new BigDecimal("2"),getVehicleCapacity(vehicleType), new BigDecimal("1"),
                getDestinationDistance(location));
        return tripRateConfig;
    }

    private BigDecimal getBusDiscount(VehicleType vehicleType) {

        if(VehicleType.BUS.equals(vehicleType)) {
            return new BigDecimal("2");
        }else {
            return new BigDecimal("0");
        }
    }

    private Integer getVehicleCapacity(VehicleType vehicleType) {

        switch (vehicleType) {
            case CAR:
                return 5;
            case SUV:
                return 7;
            case VAN:
                return 10;
            case BUS:
                return 30;
        }
        return null;
    }

    private BigDecimal getDestinationDistance(LocationEnum destination) {

        switch (destination) {
            case PUNE:
                return new BigDecimal("0");
            case MUMBAI:
                return new BigDecimal("200");
            case BANGALORE:
                return new BigDecimal("1000");
            case DELHI:
                return new BigDecimal("2050");
            case CHENNAI:
                return new BigDecimal("1234.5");
        }
        return null;
    }

}
