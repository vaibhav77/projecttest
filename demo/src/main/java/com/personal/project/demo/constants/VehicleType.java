package com.personal.project.demo.constants;

/**
 * Enum to store all vehicle types
 *
 * Copyright: NA
 * @author Vaibhav.Srivastava
 * @since 1.0.0
 */
public enum VehicleType {
    SUV,CAR,VAN,BUS;
}
