package com.personal.project.demo.constants;

 /**
 * Enum to store all Locations
 *
 * Copyright: NA
 * @author Vaibhav.Srivastava
 * @since 1.0.0
 */
public enum LocationEnum {

    MUMBAI("Mumbai"),
    PUNE("Pune"),
    BANGALORE("Bangalore"),
    CHENNAI("Chennai"),
    DELHI("Delhi");

    private String location;

    LocationEnum(String location) {
        this.location = location;
    }

    /**
     * Returns the {@link LocationEnum} enum constant having the given name.
     * @param name   the name
     * @return {@link LocationEnum}
     */
    public static LocationEnum getEnum(String name) {
        for (LocationEnum locationEnum : values()) {
            if (locationEnum.getLocation().equals(name)) {
                return locationEnum;
            }
        }
        throw new IllegalArgumentException(name);
    }

    /**
     * Get location
     *
     * @return location
     */
    public String getLocation() {
        return location;
    }
}
