package com.personal.project.demo.service.expensecalculator;

import com.personal.project.demo.constants.FuelType;
import com.personal.project.demo.constants.VehicleType;

import java.math.BigDecimal;

/**
 * This contract defines the interface to calculate expense.
 *
 * Copyright: None
 *
 * @author Vaibhav.Srivastava
 * @since 1.0.0
 */
public interface ExpenseCalculator {

    /**
     * @param vehicleType               The type of selected vehicle
     *                                  (Car/Bus/Van etc.)
     * @param fuelType                  The type of selected fuel
     *                                  (Diesel /
     *                                  Petrol).
     * @param destination               The selected destination
     * @param numberOfPeopleTravelling  The number of people
     *                                  traveling
     * @param isAirConditioningRequired The selected option of air
     *                                  condition
     * @return
     */
    BigDecimal calculateExpense(VehicleType vehicleType, FuelType
            fuelType, String destination, Integer numberOfPeopleTravelling, Boolean
                                        isAirConditioningRequired);

}
