package com.personal.project.demo.modal;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * A TripRateConfig for data projection.
 *
 * Copyright: NA
 * @author Vaibhav.Srivastava
 * @since 1.0.0
 */
public class TripRateConfig {

    private BigDecimal standardFuelRate;

    private BigDecimal dieselDiscount;

    private BigDecimal busDiscount;

    private BigDecimal acCharge;

    private Integer vehicleCapacity;

    private BigDecimal extraPersonCharge;

    private BigDecimal distance;

    /**
     * constructor for TripRateConfig
     * @param standardFuelRate        standardFuelRate
     * @param dieselDiscount           dieselDiscount
     * @param busDiscount              bus related Discount
     * @param acCharge                 extra charge for air conditioning
     * @param vehicleCapacity          vehicle capacity
     * @param extraPersonCharge        extra per person charge
     *
     */
    public TripRateConfig(BigDecimal standardFuelRate, BigDecimal dieselDiscount, BigDecimal busDiscount, BigDecimal acCharge,
                          Integer vehicleCapacity, BigDecimal extraPersonCharge, BigDecimal distance) {
        this.standardFuelRate = standardFuelRate;
        this.dieselDiscount = dieselDiscount;
        this.busDiscount = busDiscount;
        this.acCharge = acCharge;
        this.vehicleCapacity = vehicleCapacity;
        this.extraPersonCharge = extraPersonCharge;
        this.distance = distance;
    }

    /**
     * constructor for ContactNumber
     */
    public TripRateConfig() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TripRateConfig that = (TripRateConfig) o;
        return Objects.equals(standardFuelRate, that.standardFuelRate) &&
                Objects.equals(dieselDiscount, that.dieselDiscount)
                && Objects.equals(busDiscount, that.busDiscount)
                && Objects.equals(acCharge, that.acCharge)
                && Objects.equals(vehicleCapacity, that.vehicleCapacity)
                && Objects.equals(extraPersonCharge, that.extraPersonCharge)
                && Objects.equals(distance, that.distance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(standardFuelRate, dieselDiscount, busDiscount, acCharge, vehicleCapacity,
                extraPersonCharge, distance);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TripRateConfig{");
        sb.append("standardFuelRate='").append(standardFuelRate).append('\'');
        sb.append(", dieselDiscount='").append(dieselDiscount).append('\'');
        sb.append(", busDiscount='").append(busDiscount).append('\'');
        sb.append(", acCharge='").append(acCharge).append('\'');
        sb.append(", vehicleCapacity='").append(vehicleCapacity).append('\'');
        sb.append(", extraPersonCharge='").append(extraPersonCharge).append('\'');
        sb.append(", distance='").append(distance).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public BigDecimal getStandardFuelRate() {
        return standardFuelRate;
    }

    public void setStandardFuelRate(BigDecimal standardFuelRate) {
        this.standardFuelRate = standardFuelRate;
    }

    public BigDecimal getDieselDiscount() {
        return dieselDiscount;
    }

    public void setDieselDiscount(BigDecimal dieselDiscount) {
        this.dieselDiscount = dieselDiscount;
    }

    public BigDecimal getBusDiscount() {
        return busDiscount;
    }

    public void setBusDiscount(BigDecimal busDiscount) {
        this.busDiscount = busDiscount;
    }

    public BigDecimal getAcCharge() {
        return acCharge;
    }

    public void setAcCharge(BigDecimal acCharge) {
        this.acCharge = acCharge;
    }

    public Integer getVehicleCapacity() {
        return vehicleCapacity;
    }

    public void setVehicleCapacity(Integer vehicleCapacity) {
        this.vehicleCapacity = vehicleCapacity;
    }

    public BigDecimal getExtraPersonCharge() {
        return extraPersonCharge;
    }

    public void setExtraPersonCharge(BigDecimal extraPersonCharge) {
        this.extraPersonCharge = extraPersonCharge;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }
}
