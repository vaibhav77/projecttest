package com.personal.project.demo.constants;

/**
 * Enum to store all fuel types
 *
 * Copyright: NA
 * @author Vaibhav.Srivastava
 * @since 1.0.0
 */
public enum FuelType {
    PETROL, DIESEL;
}
