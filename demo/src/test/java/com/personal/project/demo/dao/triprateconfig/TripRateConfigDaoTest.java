package com.personal.project.demo.dao.triprateconfig;

import com.personal.project.demo.constants.LocationEnum;
import com.personal.project.demo.constants.VehicleType;
import com.personal.project.demo.dao.TripRateConfigDaoImpl;
import com.personal.project.demo.modal.TripRateConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import java.math.BigDecimal;


/**
 * Tests the dao for calculating TripRateConfig
 *
 * Copyright: NA
 *
 * @author Vaibhav.Srivastava
 * @since 1.0.0
 */
@SpringBootTest
public class TripRateConfigDaoTest {

    @InjectMocks
    private TripRateConfigDaoImpl tripRateConfigDao;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test that the correct TripRateConfig is returned for MumbaiCarTrip
     *
     */
    @Test
    public void givenMumbaiCarTripExpectTripRateConfig(){

        TripRateConfig expectedTripRateConfig = new TripRateConfig(new BigDecimal("15"), new BigDecimal("1"),
                new BigDecimal("0"),new BigDecimal("2"),5, new BigDecimal("1"),
                new BigDecimal("200"));

        TripRateConfig actualTripRateConfig = tripRateConfigDao.getTripDateConfig(VehicleType.CAR, LocationEnum.MUMBAI);

        Assertions.assertEquals(expectedTripRateConfig,actualTripRateConfig);
    }


    /**
     * Test that the correct TripRateConfig is returned for DelhiBusTrip
     *
     */
    @Test
    public void givenDelhiBusTripExpectTripRateConfig(){

        TripRateConfig expectedTripRateConfig = new TripRateConfig(new BigDecimal("15"), new BigDecimal("1"),
                new BigDecimal("2"),new BigDecimal("2"),30, new BigDecimal("1"),
                new BigDecimal("2050"));

        TripRateConfig actualTripRateConfig = tripRateConfigDao.getTripDateConfig(VehicleType.BUS, LocationEnum.DELHI);

        Assertions.assertEquals(expectedTripRateConfig,actualTripRateConfig);
    }


    /**
     * Test that the correct TripRateConfig is returned for ChennaiVanTrip
     *
     */
    @Test
    public void givenChennaiVanTripExpectTripRateConfig(){

        TripRateConfig expectedTripRateConfig = new TripRateConfig(new BigDecimal("15"), new BigDecimal("1"),
                new BigDecimal("0"),new BigDecimal("2"),10, new BigDecimal("1"),
                new BigDecimal("1234.5"));

        TripRateConfig actualTripRateConfig = tripRateConfigDao.getTripDateConfig(VehicleType.VAN, LocationEnum.CHENNAI);

        Assertions.assertEquals(expectedTripRateConfig,actualTripRateConfig);
    }



}