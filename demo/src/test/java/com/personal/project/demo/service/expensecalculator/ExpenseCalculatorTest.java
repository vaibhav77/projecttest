package com.personal.project.demo.service.expensecalculator;

import com.personal.project.demo.constants.FuelType;
import com.personal.project.demo.constants.LocationEnum;
import com.personal.project.demo.constants.VehicleType;
import com.personal.project.demo.dao.TripRateConfigDao;
import com.personal.project.demo.modal.TripRateConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;


/**
 * Tests the service for calculating trip Expense
 *
 * Copyright: NA
 *
 * @author Vaibhav.Srivastava
 * @since 1.0.0
 */
@SpringBootTest
public class ExpenseCalculatorTest {

    @InjectMocks
    private ExpenseCalculatorImpl eventHistoryService;

    @Mock
    private TripRateConfigDao tripRateConfigDao;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test that the correct expense amount is returned
     *
     */
    @Test
    public void givenMumbaiTripExpectExpense(){

        BigDecimal expectedExpense = new BigDecimal("3400");

        LocationEnum destination = LocationEnum.MUMBAI;

        TripRateConfig tripRateConfig = new TripRateConfig(new BigDecimal("15"), new BigDecimal("1"),
                new BigDecimal("2"),new BigDecimal("2"),5, new BigDecimal("1"),
                new BigDecimal("200"));

        Mockito.when(tripRateConfigDao.getTripDateConfig(VehicleType.CAR, destination)).thenReturn(tripRateConfig);

        BigDecimal actualExpense = eventHistoryService.calculateExpense(VehicleType.CAR, FuelType.PETROL,
                destination.getLocation(), 5,true);

        Assertions.assertEquals(expectedExpense,actualExpense);
    }


}